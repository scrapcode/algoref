/**
 * insertion.cpp
 * 
 * insertion sorting
**/

#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
    const int arraySize = 10;
    int data[ arraySize ] = { 34, 56, 4, 10, 77, 51, 93, 30, 5, 52 };
    int insert; // temp var to hold element to insert
    
    cout << "Unsorted array:" << endl;
    for( int i = 0; i < arraySize; i++ )
        cout << setw(4) << data[ i ];
        
    // insertion sort
    for( int next = 1; next < arraySize; next++ )
    {
        insert = data[ next ]; // store the element's value
        
        int moveItem = next; // initialize the location to place the element
        
        // find where to put the current element
        while( ( moveItem > 0 ) && ( data[ moveItem - 1 ] > insert ) )
        {
            // shift element to the right
            data[ moveItem ] = data[ moveItem - 1 ];
            moveItem--; // decrement moveItem
        }
        
        data[ moveItem ] = insert; // place inserted element into the array
    } // end for
    
    // output sorted array
    for( int i = 0; i < arraySize; i++ )
        cout << setw(4) << data[ i ];
        
    cout << endl; // line break
    
    return 0;
}