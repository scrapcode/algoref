/**
 * linear.cpp
 * 
 * performs a linear search on an array
 * n complexity
**/

#include<iostream>

using namespace std;

// function prototypes
int linearSearch( const int array[], int key, int sizeOfArray );

int main()
{
    const int arraySize = 100;
    int data[ arraySize ];
    int searchKey;
    
    // create data
    for( int i = 0; i < arraySize; i++ )
        data[ i ] = i * 2;
    
    cout << "Enter an integer key to search for: ";
    cin >> searchKey;
    
    int element = linearSearch( data, searchKey, arraySize );
    
    if( element != -1 )
        cout << "Found value in element #" << element << endl;
    else
        cout << "Value was not found." << endl;
}

int linearSearch( const int array[], int key, int sizeOfArray )
{
    for( int i = 0; i < sizeOfArray; i++ )
        if( array[ i ] == key )
            return i; // element was found
    
    return -1; // element was not found
}